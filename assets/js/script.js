console.log("helooooo")

function pageLoaded(){
    let canvas = document.getElementById("canvas");
    let context = canvas.getContext("2d");
    
    context.fillStyle = "#000080";
    context.fillRect(0, 0, 25, 25);
    context.fillRect(0, 475, 25, 25);
    context.fillRect(975, 0, 25, 25);
    context.fillRect(975, 475, 25, 25);

    context.beginPath();
    context.arc(400, 450, 40, 0, Math.PI, true);
    context.stroke();

    // hexagon
    context.beginPath();
    context.moveTo(92, 132);
    context.lineTo(145, 132);
    context.lineTo(169, 181);
    context.lineTo(144, 222);
    context.lineTo(87, 222);
    context.lineTo(63, 181);
    context.lineTo(92, 132);
    context.stroke();
    context.fillStyle = 'red';
    context.fill();

    // 4 star
    context.beginPath();
    context.moveTo(233, 269);
    context.lineTo(240, 313);
    context.lineTo(275, 320);
    context.lineTo(242, 330);
    context.lineTo(231, 377);
    context.lineTo(226, 329);
    context.lineTo(196, 321);
    context.lineTo(226, 312);
    context.lineTo(233, 269);
    context.stroke();
    context.fillStyle = 'orange';
    context.fill();

    // 5 star
    context.beginPath();
    context.moveTo(392, 130);
    context.lineTo(412, 192);
    context.lineTo(470, 192);
    context.lineTo(422, 219);
    context.lineTo(442, 270);
    context.lineTo(391, 235);
    context.lineTo(346, 270);
    context.lineTo(363, 219);
    context.lineTo(315, 192);
    context.lineTo(371, 192);
    context.lineTo(392, 130);
    context.stroke();
    context.fillStyle = 'yellow';
    context.fill();

    // triangle
    context.beginPath();
    context.lineTo(746, 94);
    context.lineTo(800, 175);
    context.lineTo(700, 177);
    context.lineTo(746, 94);
    context.stroke();
    context.fillStyle = 'green';
    context.fill();

    // hexagon 2
    context.beginPath();
    context.moveTo(592, 282);
    context.lineTo(645, 282);
    context.lineTo(669, 321);
    context.lineTo(644, 372);
    context.lineTo(587, 372);
    context.lineTo(563, 321);
    context.lineTo(592, 282);
    context.stroke();
    context.fillStyle = 'blue';
    context.fill();



    context.font = "25px Calibri";
    context.fillText("I got you a kaleidoscope at the airport today!", 45, 50);

    context.fillStyle = "red";
    context.font = "25px Comic Sans MS";
    context.fillText("A kaleidoscope? I'm not 5...", 400, 80);

    context.font = "35px Helvetica";
    context.fillText("shapes and colours ooOOooOOOo", 450, 450);
    
}

pageLoaded()